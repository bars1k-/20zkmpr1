package com.company;

public interface ICounter {
    void IncCounter();
    void DecCounter();
}
