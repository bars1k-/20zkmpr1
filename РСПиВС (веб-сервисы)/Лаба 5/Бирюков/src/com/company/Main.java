package com.company;

public class Main {

    public static void main(String[] args) {
        Counter a = new Counter();
        System.out.println(a.getCounter());
        a.IncCounter();
        System.out.println(a.getCounter());
        a.DecCounter();
        System.out.println(a.getCounter());
        Counter b = new Counter(322);
        System.out.println(b.getCounter());
        b.IncCounter();
        System.out.println(b.getCounter());
        b.DecCounter();
        System.out.println(b.getCounter());
    }
}
