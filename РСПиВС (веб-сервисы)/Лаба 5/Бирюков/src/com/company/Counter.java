package com.company;

public class Counter implements ICounter {
    private int c;
    public Counter () {
        c = 0;
    }
    public Counter (int n) {
        c = n;
    }
    public int getCounter() {
        return c;
    }

    @Override
    public void IncCounter() {
        c += 1;
    }
    @Override
    public void DecCounter() {
        c -= 1;
    }
}
