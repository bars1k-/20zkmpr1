package com.company;

import java.util.Objects;

public class Point {
    private int X;
    private int Y;
    Point(int x, int y) {
        X = x;
        Y = y;
    }
    Point() {
        this(0, 0);
    }

    public int getX() {
        return X;
    }
    public int getY(){
        return Y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return X == point.X && Y == point.Y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(X, Y);
    }
}
