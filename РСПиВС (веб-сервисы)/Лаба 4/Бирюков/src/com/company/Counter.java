package com.company;

public class Counter {
    private int c;
    public Counter () {
        c = 0;
    }
    public Counter (int n) {
        c = n;
    }
    public int getCounter() {
        return c;
    }
    public void IncCounter() {
        c += 1;
    }
    public void DecCounter() {
        c -= 1;
    }
}
