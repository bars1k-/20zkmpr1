package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[][] array = Randomize();
        String s = "";
        for (int i = 0; i < array[2].length; i++) {
            s += String.format("%d ", array[2][i]);
        }
        System.out.println(s);
    }

    public static int[][] Randomize() {
        int[][] array = new int [5][6];
        final Random r = new Random();
        for (int i = 0; i < array.length; i++) { //array.length выведет изначально длину первого измерения.
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = r.nextInt(100); //диапазон [0; 100)
            }
        }
        return array;
    }
}
