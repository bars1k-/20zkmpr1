package com.company;

public class Main {

    public static String getPrime() {
        String result = "";
        for (int i = 2; i <= 100; i++) {
            if (isAPrime(i))
                result += String.format("%d ", i);
        }
        return result;
    }

    public static boolean isAPrime(int i) {
        for (int j = 2; j <= i/2; j++) {
            if (i % j == 0)
                return false;
        }
        return true;
    }
    public static void main(String[] args) {
        System.out.println("Простые числа от 2 до 100: \n" + getPrime());
    }
}
